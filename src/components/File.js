import React, { Component } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import colors from "../utils/colors";

const gray_square = require("../images/gray_square_icon.png");

export default class File extends Component {
  render() {
    return (
      <View style={{ flexDirection: "row" }}>
        <View style={styles.verticalDivider} />
        <View style={styles.horizontalDivider} />
        <Image source={gray_square} style={styles.icon} />
        <Text style={styles.text}>{this.props.name}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  horizontalDivider: {
    backgroundColor: colors.black,
    height: 1,
    width: 10,
    alignSelf: "center"
  },
  verticalDivider: {
    backgroundColor: colors.black,
    height: 40,
    width: 1
  },
  text: {
    alignSelf: "center"
  },
  icon: { marginTop: 12, width: 15, height: 15, marginRight: 5 }
});
