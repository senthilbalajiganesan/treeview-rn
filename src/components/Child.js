import React, { Component } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import Folder from "./Folder";
import File from "./File";
import colors from "../utils/colors";

class Child extends Component {
  render() {
    return (
      <View>
        {this.props.data.map((item, key) => {
          return item.type === "folder" ? (
            <Folder data={item} key={key} />
          ) : (
            <File name={item.name} key={key} />
          );
        })}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  horizontalDivider: {
    backgroundColor: colors.black,
    height: 1,
    width: 10,
    alignSelf: "center"
  },
  verticalDivider: {
    backgroundColor: colors.black,
    height: 40,
    width: 1
  },
  text: {
    alignSelf: "center",
    color: colors.blue,
    textDecorationLine: "underline"
  },
  icon: { marginTop: 12, width: 15, height: 15, marginRight: 5 }
});
export default Child;
