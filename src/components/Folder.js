import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Modal,
  ToastAndroid
} from "react-native";
import Child from "./Child";
import colors from "../utils/colors";
import AddNewFile from "./AddNewFile";

const plus_icon = require("../images/plus_square_icon.jpg");
const minus_icon = require("../images/minus_square_icon.jpg");
const gray_square = require("../images/gray_square_icon.png");

class Folder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      showAddFileModal: false
    };
  }
  render() {
    const { data } = this.state;
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          onRequestClose={() => this.setState({ showAddFileModal: false })}
          visible={this.state.showAddFileModal}
        >
          <AddNewFile
            onAddnew={text => {
              this.setState({ showAddFileModal: false });
              if (text !== "") {
                let newData = Object.assign(data, {});
                newData.children.push({
                  type: "file",
                  name: text,
                  path: `/animals/cat/images/${text}`
                });
                this.setState({ data: newData });
              } else {
                ToastAndroid.show(
                  "Please enter valid file name",
                  ToastAndroid.SHORT
                );
              }
            }}
          />
        </Modal>
        <View style={{ flexDirection: "row" }}>
          <View
            style={{
              backgroundColor: colors.black,
              height: data.is_expanded ? data.children.length * 40 : 40,
              width: 1
            }}
          />
          <View style={styles.horizontalDividerFolder} />
          <Image
            source={data.is_expanded ? minus_icon : plus_icon}
            style={styles.icon}
          />
          <View>
            <TouchableWithoutFeedback
              onPress={() => {
                let newData = Object.assign(data, {});
                newData.is_expanded = !data.is_expanded;
                this.setState({ data: newData });
              }}
            >
              <View style={{ padding: 5 }}>
                <Text>{data.name}</Text>
              </View>
            </TouchableWithoutFeedback>
            {data.is_expanded ? <Child data={data.children} /> : <View />}
          </View>
        </View>
        <TouchableWithoutFeedback
          onPress={() => this.setState({ showAddFileModal: true })}
        >
          <View style={{ flexDirection: "row" }}>
            <View style={styles.verticalDivider} />
            <View style={styles.horizontalDivider} />
            <Image source={gray_square} style={styles.grayIcon} />
            <Text style={styles.text}>Add New</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  horizontalDividerFolder: {
    backgroundColor: colors.black,
    height: 1,
    width: 10,
    marginTop: 15
  },
  icon: { marginTop: 8, width: 15, height: 15, marginRight: 5 },
  horizontalDivider: {
    backgroundColor: colors.black,
    height: 1,
    width: 10,
    alignSelf: "center"
  },
  verticalDivider: {
    backgroundColor: colors.black,
    height: 40,
    width: 1
  },
  text: {
    alignSelf: "center",
    color: colors.blue,
    textDecorationLine: "underline"
  },
  grayIcon: { marginTop: 12, width: 15, height: 15, marginRight: 5 }
});

export default Folder;
