import React, { Component } from "react";
import { View, Text, TextInput, TouchableNativeFeedback } from "react-native";
import ModalViewsBG from "./ModalViewsBG";

class AddNewFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ""
    };
  }
  render() {
    return (
      <ModalViewsBG>
        <View style={{ padding: 20 }}>
          <TextInput
            style={{ padding: 10, backgroundColor: "#FFF" }}
            placeholder={"Enter file name here"}
            onChangeText= {text => this.setState({ text })}
          />
          <TouchableNativeFeedback
            onPress={() => this.props.onAddnew(this.state.text)}
          >
            <View style={{ padding: 10, alignItems: "center" }}>
              <Text style={{ fontWeight: "bold" }}>Add File</Text>
            </View>
          </TouchableNativeFeedback>
        </View>
      </ModalViewsBG>
    );
  }
}
export default AddNewFile;
