import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableWithoutFeedback,ScrollView } from "react-native";
import Data from "./Data";
import Child from "./components/Child";
import colors from "./utils/colors";

export default class Main extends Component<{}> {
  render() {
    return (
      <ScrollView containerStyle={styles.container}>
        <View style={styles.headerView}>
          <Text style={styles.headerText}>Tree View component</Text>
        </View>
        <Child data={Data} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "#F5FCFF",
    padding: 30
  },
  headerView: { backgroundColor: colors.black, padding: 5, borderRadius: 10 },
  headerText: { color: colors.white, fontWeight: "bold" }
});
