export default [
  {
    type: "folder",
    name: "animals",
    path: "/animals",
    is_expanded: false,
    children: [
      {
        type: "folder",
        name: "cat",
        path: "/animals/cat",
        is_expanded: false,
        children: [
          {
            type: "folder",
            name: "images",
            path: "/animals/cat/images",
            is_expanded: false,
            children: [
              {
                type: "file",
                name: "cat001.jpg",
                path: "/animals/cat/images/cat001.jpg"
              },
              {
                type: "file",
                name: "cat001.jpg",
                path: "/animals/cat/images/cat002.jpg"
              }
            ]
          }
        ]
      }
    ]
  }
];
